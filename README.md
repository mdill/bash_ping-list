# BASH Ping IP Tool

## Purpose

This is a simple BASH tool which accepts a list of IP addresses (last octet) to
ping, and returns the reply status of each address.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/bash_ping-list.git

## Placement

It is suggested that this script be placed in `/usr/local/bin/` for simplicity
and ease-of-use:

    # cp ~/bash_ping-list/ping-list /usr/local/bin/

Once this is done, it can be made executable by one of the following:

    # chmod +x /usr/local/bin/ping-list
    $ chmod +x ~/bash_ping-list/ping-list

If you choose not to make this a global function, you can execute the script
directly from the cloned GIT directory:

    $ ~/bash_ping-list/ping-list

## Use

In order to use this script, you must modify two elements of the code:
    firstOctets
    ourArray

The `firstOctets` is a string of the first three octets of the IP you want to
ping.  The `ourArray` is an array of octets for individual IPs you want to ping.
In other words, if you want to ping a list of:

    192.168.1.56
    192.168.1.123
    192.168.1.124
    192.168.1.161

Then your `firstOctets` and your `ourArray` would look like:

    firstOctets="192.168.1"
    ourArray=(
              56
              123
              124
              161
            )

The number of elements in `ourArray` is handled by the script, and is effectively
limitless (depending on how many lines your terminal windows supports).  Once
these two variables are configured, running the script will continuously ping the
listed addresses and return a good (checkmark) or bad (X) status for their
connections.

In order to quit the program, a simpl `q` input will end the test and cleanup the
workspace.

## License

This project is licensed under the BSD License - see the [LICENSE.md](LICENSE.txt) file for
details.

